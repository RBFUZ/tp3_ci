package test.java;

import org.hamcrest.*;
import static org.junit.Assert.*;

import java.sql.Date;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import main.java.tp3.GestionJustice;
import main.java.tp3.IFT287Exception;
import main.java.tp3.Launcher;
import main.java.tp3.TupleAvocat;
import main.java.tp3.TupleJuge;
import main.java.tp3.TupleJury;
import main.java.tp3.TuplePartie;
import main.java.tp3.TupleProces;
import main.java.tp3.TupleSeance;

public class IntegrationTest
{
    static GestionJustice gestionJustice;

    static final String serveur = new String("externe");
    static final String bd = new String("tetoyejf");
    static final String user = new String("tetoyejf");
    static final String pass = new String("CUYSqLY14GNzWS_PWjBBBXWtYL12JNrQ");

    static TupleAvocat tupleAvocat1;
    static TupleAvocat tupleAvocat2;
    static TupleJuge tupleJuge1;
    static TupleJuge tupleJuge2;
    static TupleJuge tupleJuge3;
    static TupleJury tupleJury1;
    static TupleJury tupleJury2;
    static TuplePartie tuplePartie1;
    static TuplePartie tuplePartie2;
    static TupleProces tupleProces1;
    static TupleProces tupleProces2;
    static TupleProces tupleProces3;
    static TupleSeance tupleSeance1;
    static TupleSeance tupleSeance2;

    static java.util.Date dateUtilFutur;
    static java.util.Date dateUtilPasse;

    static java.sql.Date dateSQLFutur;
    static java.sql.Date dateSQLPasse;

    /**
     * @throws java.lang.Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception
    {
        gestionJustice = new GestionJustice(serveur, bd, user, pass);

        // Clear de la base de données
        clearDatabase();

        DateFormat df = new SimpleDateFormat("dd MM yyyy");
        dateUtilFutur = df.parse("30 12 2017");
        dateUtilPasse = df.parse("31 08 2017");

        dateSQLFutur = new java.sql.Date(dateUtilFutur.getTime());
        dateSQLPasse = new java.sql.Date(dateUtilPasse.getTime());

        tupleAvocat1 = new TupleAvocat(72, "Bernard", "Macron", 0);
        tupleAvocat2 = new TupleAvocat(23, "Didier", "Bernodo", 1);

        tupleJuge1 = new TupleJuge(27, "Baba", "Filipo", 35);
        tupleJuge2 = new TupleJuge(28, "Udu", "Bobo", 34);
        tupleJuge3 = new TupleJuge(29, "Boba", "Bubu", 87);

        tupleJury1 = new TupleJury(123456789, "Bebere", "Paulus", "M", 26);
        tupleJury2 = new TupleJury(987654321, "Ana", "Fouchou", "F", 24);

        tuplePartie1 = new TuplePartie(26, "Christophe", "LeVrai", 72);
        tuplePartie2 = new TuplePartie(29, "Didier", "LeFaux", 23);

        tupleProces1 = new TupleProces(2, 29, (java.sql.Date) dateSQLPasse, 1, 26, 29, -1);
        tupleProces2 = new TupleProces(3, 28, (java.sql.Date) dateSQLPasse, 1, 26, 29, -1);

        tupleSeance1 = new TupleSeance(2, 2, dateSQLFutur);
        tupleSeance2 = new TupleSeance(3, 4, dateSQLPasse);
        
        ajoutAvocatsBD();
        ajoutJugesBD();
        ajoutPartiesBD();
        ajoutProcesBD();
    }

    /**
     * Clear la base de données
     * 
     * @throws SQLException
     */
    private static void clearDatabase() throws SQLException
    {
        gestionJustice.getCx().getConnection().prepareStatement("TRUNCATE \"Avocat\" CASCADE;").executeUpdate();
        gestionJustice.getCx().getConnection().prepareStatement("TRUNCATE \"Jury\" CASCADE;").executeUpdate();
        gestionJustice.getCx().getConnection().prepareStatement("TRUNCATE \"Juge\" CASCADE;").executeUpdate();
        gestionJustice.getCx().getConnection().prepareStatement("TRUNCATE \"JuryProces\" CASCADE;").executeUpdate();
        gestionJustice.getCx().getConnection().prepareStatement("TRUNCATE \"Proces\" CASCADE;").executeUpdate();
        gestionJustice.getCx().getConnection().prepareStatement("TRUNCATE \"Partie\" CASCADE;").executeUpdate();
        gestionJustice.getCx().getConnection().prepareStatement("TRUNCATE \"Seance\" CASCADE;").executeUpdate();
        gestionJustice.getCx().getConnection().commit();
    }

    /**
     * Ajout des procès dans la BD
     */
    private static void ajoutProcesBD()
    {
        try
        {
            gestionJustice.getGestionProces().creer(tupleProces1);
        }
        catch (Exception e)
        {
            fail();
        }

        try
        {
            gestionJustice.getGestionProces().creer(tupleProces2);
        }
        catch (Exception e)
        {
            fail();
        }
    }

    /**
     * Ajout des parties dans la BD
     */
    private static void ajoutPartiesBD()
    {
        try
        {
            gestionJustice.getGestionPartie().ajout(tuplePartie1);
        }
        catch (Exception e)
        {
            fail();
        }

        try
        {
            gestionJustice.getGestionPartie().ajout(tuplePartie2);
        }
        catch (Exception e)
        {
            fail();
        }
    }

    /**
     * Ajout des avocats dans la BD
     */
    private static void ajoutAvocatsBD()
    {
        try
        {
            gestionJustice.getGestionAvocat().ajouter(tupleAvocat1);
        }
        catch (Exception e)
        {
            fail();
        }

        try
        {
            gestionJustice.getGestionAvocat().ajouter(tupleAvocat2);
        }
        catch (Exception e)
        {
            fail();
        }
    }

    /**
     * Ajout des juges dans la BD
     */
    private static void ajoutJugesBD()
    {
        try
        {
            gestionJustice.getGestionJuge().ajouter(tupleJuge1);
        }
        catch (Exception e)
        {
            fail();
        }

        try
        {
            gestionJustice.getGestionJuge().ajouter(tupleJuge2);
        }
        catch (Exception e)
        {
            fail();
        }

        try
        {
            gestionJustice.getGestionJuge().ajouter(tupleJuge3);
        }
        catch (Exception e)
        {
            fail();
        }
    }

    @Test
    public void terminerProces() throws Exception
    {
        try
        {
            assertFalse(gestionJustice.getGestionJuge().estDisponible(tupleJuge3));
            gestionJustice.getGestionProces().terminer(tupleProces1, 1);
        }
        catch (Exception e)
        {
            fail();
        }

        try
        {
            assertEquals(gestionJustice.getGestionProces().affichage(tupleProces1).getDecision(), 1);
            assertTrue(gestionJustice.getGestionJuge().estDisponible(tupleJuge3));
        }
        catch (Exception e)
        {
            fail();
        }
    }
    
    @Test(expected = IFT287Exception.class)
    public void terminerProcesDejaTerminer() throws Exception
    {
        assertTrue(gestionJustice.getGestionJuge().estDisponible(tupleJuge3));
        gestionJustice.getGestionProces().terminer(gestionJustice.getGestionProces().affichage(tupleProces1), 1);

        assertEquals(gestionJustice.getGestionProces().affichage(tupleProces1).getDecision(), 1);
        assertTrue(gestionJustice.getGestionJuge().estDisponible(tupleJuge3));
    }

    @Test
    public void ajouterSeanceAProces()
    {
        try
        {
            assertTrue(gestionJustice.getGestionSeance().affichage(tupleProces2).isEmpty());

            gestionJustice.getGestionSeance().ajout(new TupleSeance(1, 3, dateSQLFutur));

            assertEquals(gestionJustice.getGestionSeance().affichage(tupleProces2).get(0).getId(), 1);
            assertTrue(gestionJustice.getGestionSeance().affichage(tupleProces2).get(0).getDate().equals(dateSQLFutur));
        }
        catch (Exception e)
        {
            e.printStackTrace();
            fail();
        }
    }
    
    @Test(expected = IFT287Exception.class)
    public void ajouterSeanceProcesTermine() throws Exception
    {
        gestionJustice.getGestionProces().terminer(gestionJustice.getGestionProces().affichage(new TupleProces(3)), 1);
        gestionJustice.getGestionSeance().ajout(new TupleSeance(2, 3, dateSQLFutur));
    }
    
    @AfterClass
    public static void logout()
    {
        if (gestionJustice != null)
        {
            try
            {
                clearDatabase();
                gestionJustice.fermer();
            }
            catch (SQLException e)
            {
                fail();
            }
        }
    }
}