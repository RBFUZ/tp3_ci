package test.java;

import org.hamcrest.*;
import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import main.java.tp3.IFT287Exception;
import main.java.tp3.Launcher;
import main.java.tp3.TupleAvocat;
import main.java.tp3.TupleJuge;
import main.java.tp3.TupleJury;
import main.java.tp3.TuplePartie;
import main.java.tp3.TupleProces;
import main.java.tp3.TupleSeance;

public class FunctionalTest
{
    static final String serveur = new String("externe");
    static final String bd = new String("tetoyejf");
    static final String user = new String("tetoyejf");
    static final String pass = new String("CUYSqLY14GNzWS_PWjBBBXWtYL12JNrQ");
    static final String fichier = new String("tp2.dat");
    private static Launcher instanceLauncher = null;
    private static String[] args = new String[5];
    private static BufferedReader reader;

    static TupleAvocat tupleAvocat1;
    static TupleAvocat tupleAvocat2;
    static TupleJuge tupleJuge1;
    static TupleJuge tupleJuge2;
    static TupleJuge tupleJuge3;
    static TupleJuge tupleJuge4;
    static TupleJuge tupleJuge5;
    static TupleJury tupleJury1;
    static TupleJury tupleJury2;
    static TuplePartie tuplePartie1;
    static TuplePartie tuplePartie2;
    static TupleProces tupleProces1;
    static TupleProces tupleProces2;
    static TupleProces tupleProces3;
    static TupleSeance tupleSeance1;
    static TupleSeance tupleSeance2;
    
    static java.util.Date dateUtilFutur;
    static java.util.Date dateUtilPasse;

    static java.sql.Date dateSQLFutur;
    static java.sql.Date dateSQLPasse;
    
    /**
     * @throws java.lang.Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception
    {
        // Permet de simuler les arguments de l'utilisateur
        simulationArgs();

        if (args.length < 4)
        {
            throw new Exception("Erreur dans la création du tableau");
        }

        instanceLauncher = new Launcher(args[0], args[1], args[2], args[3]);
        
        // Clear de la base de données
        clearDatabase();
        
        reader = Launcher.ouvrirFichier(args);
        
        DateFormat df = new SimpleDateFormat("dd MM yyyy");
        dateUtilFutur = df.parse("30 12 2017");
        dateUtilPasse = df.parse("31 08 2017");

        dateSQLFutur = new java.sql.Date(dateUtilFutur.getTime());
        dateSQLPasse = new java.sql.Date(dateUtilPasse.getTime());

        tupleAvocat1 = new TupleAvocat(72, "Bernard", "Macron", 0);
        tupleAvocat2 = new TupleAvocat(23, "Didier", "Bernodo", 1);

        tupleJuge1 = new TupleJuge(27, "Baba", "Filipo", 35);
        tupleJuge2 = new TupleJuge(28, "Udu", "Bobo", 34);
        tupleJuge3 = new TupleJuge(29, "Boba", "Bubu", 87);
        tupleJuge4 = new TupleJuge(2, "John", "Doe", 58);
        tupleJuge5 = new TupleJuge(3, "John", "Doe", 58);        

        tupleJury1 = new TupleJury(123456789, "Bebere", "Paulus", "M", 26);
        tupleJury2 = new TupleJury(987654321, "Ana", "Fouchou", "F", 24);

        tuplePartie1 = new TuplePartie(26, "Christophe", "LeVrai", 72);
        tuplePartie2 = new TuplePartie(29, "Didier", "LeFaux", 23);

        tupleProces1 = new TupleProces(2, 29, (java.sql.Date) dateSQLPasse, 1, 26, 29, -1);
        tupleProces2 = new TupleProces(3, 28, (java.sql.Date) dateSQLPasse, 1, 26, 29, -1);
        tupleProces3 = new TupleProces(4, 28, (java.sql.Date) dateSQLPasse, 1, 26, 29, -1);

        tupleSeance1 = new TupleSeance(2, 2, dateSQLFutur);
        tupleSeance2 = new TupleSeance(3, 4, dateSQLPasse);
        
        ajoutAvocatsBD();
        ajoutJugesBD();
        ajoutPartiesBD();
        ajoutProcesBD();
    }
    
    /**
     * Ajout des procès dans la BD
     */
    private static void ajoutProcesBD()
    {
        try
        {
            Launcher.getGestionJustice().getGestionProces().creer(tupleProces1);
        }
        catch (Exception e)
        {
            fail();
        }

        try
        {
            Launcher.getGestionJustice().getGestionProces().creer(tupleProces2);
        }
        catch (Exception e)
        {
            fail();
        }
        
        try
        {
            Launcher.getGestionJustice().getGestionProces().creer(tupleProces3);
        }
        catch (Exception e)
        {
            fail();
        }
    }

    /**
     * Ajout des parties dans la BD
     */
    private static void ajoutPartiesBD()
    {
        try
        {
            Launcher.getGestionJustice().getGestionPartie().ajout(tuplePartie1);
        }
        catch (Exception e)
        {
            fail();
        }

        try
        {
            Launcher.getGestionJustice().getGestionPartie().ajout(tuplePartie2);
        }
        catch (Exception e)
        {
            fail();
        }
    }

    /**
     * Ajout des avocats dans la BD
     */
    private static void ajoutAvocatsBD()
    {
        try
        {
            Launcher.getGestionJustice().getGestionAvocat().ajouter(tupleAvocat1);
        }
        catch (Exception e)
        {
            fail();
        }

        try
        {
            Launcher.getGestionJustice().getGestionAvocat().ajouter(tupleAvocat2);
        }
        catch (Exception e)
        {
            fail();
        }
    }

    /**
     * Ajout des juges dans la BD
     */
    private static void ajoutJugesBD()
    {
        try
        {
            Launcher.getGestionJustice().getGestionJuge().ajouter(tupleJuge1);
        }
        catch (Exception e)
        {
            fail();
        }

        try
        {
            Launcher.getGestionJustice().getGestionJuge().ajouter(tupleJuge2);
        }
        catch (Exception e)
        {
            fail();
        }

        try
        {
            Launcher.getGestionJustice().getGestionJuge().ajouter(tupleJuge3);
        }
        catch (Exception e)
        {
            fail();
        }
    }
    
    /**
     * Permet de simuler les arguments de l'utilisateur
     */
    private static void simulationArgs()
    {
        args[0] = new String(serveur);
        args[1] = new String(bd);
        args[2] = new String(user);
        args[3] = new String(pass);
        args[4] = new String(fichier);
    }

    /**
     * Clear la base de données
     * 
     * @throws SQLException
     */
    private static void clearDatabase() throws SQLException
    {
        Launcher.getGestionJustice().getCx().getConnection().prepareStatement("TRUNCATE \"Avocat\" CASCADE;").executeUpdate();
        Launcher.getGestionJustice().getCx().getConnection().prepareStatement("TRUNCATE \"Jury\" CASCADE;").executeUpdate();
        Launcher.getGestionJustice().getCx().getConnection().prepareStatement("TRUNCATE \"Juge\" CASCADE;").executeUpdate();
        Launcher.getGestionJustice().getCx().getConnection().prepareStatement("TRUNCATE \"JuryProces\" CASCADE;").executeUpdate();
        Launcher.getGestionJustice().getCx().getConnection().prepareStatement("TRUNCATE \"Proces\" CASCADE;").executeUpdate();
        Launcher.getGestionJustice().getCx().getConnection().prepareStatement("TRUNCATE \"Partie\" CASCADE;").executeUpdate();
        Launcher.getGestionJustice().getCx().getConnection().prepareStatement("TRUNCATE \"Seance\" CASCADE;").executeUpdate();
        Launcher.getGestionJustice().getCx().getConnection().commit();
    }

    @Test
    public void ouvrirFichier()
    {
        try
        {
            assertTrue(reader.ready());
        }
        catch (IOException e)
        {
            e.printStackTrace();
            fail();
        }
    }
    
    @Test
    public void lectureAllTransaction()
    {
        try
        {    
            String transaction = Launcher.lireTransaction(reader);
            while (!Launcher.finTransaction(transaction))
            {
                Launcher.executerTransaction(transaction);
                transaction = Launcher.lireTransaction(reader);
            }
        }
        catch (IFT287Exception e)
        {
            e.printStackTrace();
            fail();
        }
        catch (Exception e)
        {
            e.printStackTrace();
            fail();
        }
    }
    
    @Test
    public void affichageProces()
    {
        TupleProces affichageProces = null;
        try
        {
            affichageProces = Launcher.getGestionJustice().getGestionProces().affichage(new TupleProces(1));
        }
        catch (Exception e)
        {
            e.printStackTrace();
            fail();
        }
        
        assertTrue(affichageProces != null);
        assertEquals(affichageProces.getId(), 1);
        assertEquals(affichageProces.getJuge_id(), 2);
    }
    
    @Test(expected = IFT287Exception.class)
    public void affichageProcesInexistant() throws Exception
    {
        Launcher.getGestionJustice().getGestionProces().affichage(new TupleProces(90));
    }
    
    @Test
    public void creerProces()
    {
        String transaction1 = "ajouterAvocat 4 dsqd Pole 0";
        String transaction2 = "ajouterAvocat 5 gdsgd Pain 1";
        String transaction3 = "ajouterPartie 4 Don Corleone 4";
        String transaction4 = "ajouterPartie 5 FSQD fdsfsd 5";
        String transaction5 = "ajouterJuge 3 John Doe 58";
        String transaction6 = "creerProces 10 3 2017-10-02 0 4 5";
        
        try
        {
            Launcher.executerTransaction(transaction1);
            Launcher.executerTransaction(transaction2);
            Launcher.executerTransaction(transaction3);
            Launcher.executerTransaction(transaction4);
            Launcher.executerTransaction(transaction5);
            Launcher.executerTransaction(transaction6);
        }
        catch (IFT287Exception e)
        {
            e.printStackTrace();
            fail();
        }
        catch (Exception e)
        {
            e.printStackTrace();
            fail();
        }
        
        try
        {
            assertEquals(Launcher.getGestionJustice().getGestionProces().affichage(new TupleProces(10)).getId(), 10);
            assertEquals(Launcher.getGestionJustice().getGestionProces().affichage(new TupleProces(10)).getDecision(), -1);
            assertEquals(Launcher.getGestionJustice().getGestionProces().affichage(new TupleProces(10)).getPartieDefenderesse_id(), 4);
            assertEquals(Launcher.getGestionJustice().getGestionProces().affichage(new TupleProces(10)).getPartiePoursuivant_id(), 5);
            assertEquals(Launcher.getGestionJustice().getGestionProces().affichage(new TupleProces(10)).getJuge_id(), 3);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            fail();
        }
    }
    
    @Test(expected = IFT287Exception.class)
    public void creerProcesExistant() throws Exception
    {
        TupleProces tupleProces1 = Launcher.getGestionJustice().getGestionProces().affichage(new TupleProces(1));
        Launcher.getGestionJustice().getGestionProces().creer(tupleProces1);
    }
        
    @Test(expected = IFT287Exception.class)
    public void gestionAvocatAlready() throws Exception
    {
        Launcher.getGestionJustice().getGestionAvocat().ajouter(tupleAvocat1);
    }

    @Test(expected = IFT287Exception.class)
    public void gestionJugeAlready() throws Exception
    {
        Launcher.getGestionJustice().getGestionJuge().ajouter(tupleJuge1);
    }

    @Test(expected = IFT287Exception.class)
    public void ajoutProcesNull() throws Exception
    {
        Launcher.getGestionJustice().getGestionProces()
                .creer(new TupleProces(72, 23, dateSQLFutur, 0, tuplePartie1.getId(), tuplePartie2.getId(), -1));
    }

    @Test
    public void gestionProces()
    {
        try
        {
            assertTrue(Launcher.getGestionJustice().getGestionProces().affichage(tupleProces1).equals(tupleProces1));
            assertTrue(Launcher.getGestionJustice().getGestionProces().affichage(tupleProces2).equals(tupleProces2));
            assertTrue(Launcher.getGestionJustice().getGestionProces().affichage(tupleProces3).equals(tupleProces3));
        }
        catch (Exception e)
        {
            e.printStackTrace();
            fail();
        }
    }

    @Test(expected = IFT287Exception.class)
    public void gestionProcesAlready() throws Exception
    {
        Launcher.getGestionJustice().getGestionProces().creer(tupleProces1);
    }
    
    @AfterClass 
    public static void logout() {
        if (instanceLauncher != null)
        {
            try
            {
                clearDatabase();
                instanceLauncher.fermer();
            }
            catch (SQLException e)
            {
                e.printStackTrace();
                fail();
            }
        }
    }
}