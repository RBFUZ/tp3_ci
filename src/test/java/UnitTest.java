package test.java;

import org.hamcrest.*;
import static org.junit.Assert.*;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import main.java.tp3.GestionJustice;
import main.java.tp3.IFT287Exception;
import main.java.tp3.Launcher;
import main.java.tp3.TableAvocat;
import main.java.tp3.TableJuge;
import main.java.tp3.TupleAvocat;
import main.java.tp3.TupleJuge;
import main.java.tp3.TupleJury;
import main.java.tp3.TuplePartie;
import main.java.tp3.TupleProces;
import main.java.tp3.TupleSeance;

public class UnitTest
{
    // Déclaration des attributs à tester
    static TupleAvocat tupleAvocat1;
    static TupleAvocat tupleAvocat2;
    static TupleJuge tupleJuge1;
    static TupleJuge tupleJuge2;
    static TupleJury tupleJury1;
    static TupleJury tupleJury2;
    static TuplePartie tuplePartie1;
    static TuplePartie tuplePartie2;
    static TupleProces tupleProces1;
    static TupleProces tupleProces2;
    static TupleProces tupleProces3;
    static TupleSeance tupleSeance1;
    static TupleSeance tupleSeance2;

    static java.util.Date dateUtilFutur;
    static java.util.Date dateUtilPasse;

    static java.sql.Date dateSQLFutur;
    static java.sql.Date dateSQLPasse;

    /**
     * @throws java.lang.Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception
    {
        DateFormat df = new SimpleDateFormat("dd MM yyyy");
        dateUtilFutur = df.parse("30 12 2017");
        dateUtilPasse = df.parse("31 08 2017");

        dateSQLFutur = new java.sql.Date(dateUtilFutur.getTime());
        dateSQLPasse = new java.sql.Date(dateUtilPasse.getTime());

        tupleAvocat1 = new TupleAvocat(72, "Bernard", "Macron", 0);
        tupleAvocat2 = new TupleAvocat(23, "Didier", "Bernodo", 1);

        tupleJuge1 = new TupleJuge(34, "Baba", "Filipo", 35);
        tupleJuge2 = new TupleJuge(28, "Udu", "Bobo", 34);

        tupleJury1 = new TupleJury(123456789, "Bebere", "Paulus", "M", 26);
        tupleJury2 = new TupleJury(987654321, "Ana", "Fouchou", "F", 24);

        tuplePartie1 = new TuplePartie(26, "Christophe", "LeVrai", 72);
        tuplePartie2 = new TuplePartie(29, "Didier", "LeFaux", 23);

        tupleProces1 = new TupleProces(2, 34, (java.sql.Date) dateSQLFutur, 1, 26, 29, 0);
        tupleProces2 = new TupleProces(4, 28, (java.sql.Date) dateSQLPasse, 0, 26, 29, 1);
        tupleProces3 = new TupleProces(5, 28, (java.sql.Date) dateSQLPasse, 0, 26, 29, -1);

        tupleSeance1 = new TupleSeance(2, 2, dateSQLFutur);
        tupleSeance2 = new TupleSeance(3, 4, dateSQLPasse);
    }

    @Test
    public void creerAvocat()
    {
        assertEquals(tupleAvocat1.getId(), 72);
        assert (tupleAvocat1.getPrenom().equals("Bernard"));
        assert (tupleAvocat1.getNom().equals("Macron"));
        assertEquals(tupleAvocat1.getType(), 0);

        assertEquals(tupleAvocat2.getId(), 23);
        assert (tupleAvocat2.getPrenom().equals("Didier"));
        assert (tupleAvocat2.getNom().equals("Bernodo"));
        assertEquals(tupleAvocat2.getType(), 1);
    }

    @Test
    public void creerJuge()
    {
        assertEquals(tupleJuge1.getId(), 34);
        assert (tupleJuge1.getPrenom().equals("Baba"));
        assert (tupleJuge1.getNom().equals("Filipo"));
        assertEquals(tupleJuge1.getAge(), 35);

        assertEquals(tupleJuge2.getId(), 28);
        assert (tupleJuge2.getPrenom().equals("Udu"));
        assert (tupleJuge2.getNom().equals("Bobo"));
        assertEquals(tupleJuge2.getAge(), 34);
    }

    @Test
    public void creerJury()
    {
        assertEquals(tupleJury1.getNas(), 123456789);
        assert (tupleJury1.getPrenom().equals("Bebere"));
        assert (tupleJury1.getNom().equals("Paulus"));
        assert (tupleJury1.getSexe().equals("M"));
        assertEquals(tupleJury1.getAge(), 26);

        assertEquals(tupleJury2.getNas(), 987654321);
        assert (tupleJury2.getPrenom().equals("Ana"));
        assert (tupleJury2.getNom().equals("Fouchou"));
        assert (tupleJury2.getSexe().equals("F"));
        assertEquals(tupleJury2.getAge(), 24);
    }

    @Test
    public void creerPartie()
    {
        assertEquals(tuplePartie1.getId(), 26);
        assert (tuplePartie1.getPrenom().equals("Christophe"));
        assert (tuplePartie1.getNom().equals("LeVrai"));
        assertEquals(tuplePartie1.getAvocat_id(), 72);

        assertEquals(tuplePartie2.getId(), 29);
        assert (tuplePartie2.getPrenom().equals("Didier"));
        assert (tuplePartie2.getNom().equals("LeFaux"));
        assertEquals(tuplePartie2.getAvocat_id(), 23);
    }

    @Test
    public void creerProces()
    {
        assertEquals(tupleProces1.getId(), 2);
        assertEquals(tupleProces1.getJuge_id(), 34);
        assert (tupleProces1.getDate().equals(dateSQLFutur));
        assertEquals(tupleProces1.getDevantJury(), 1);
        assertEquals(tupleProces1.getPartieDefenderesse_id(), 26);
        assertEquals(tupleProces1.getPartiePoursuivant_id(), 29);
        assertEquals(tupleProces1.getDecision(), 0);

        assertEquals(tupleProces2.getId(), 4);
        assertEquals(tupleProces2.getJuge_id(), 28);
        assert (tupleProces2.getDate().equals(dateSQLPasse));
        assertEquals(tupleProces2.getDevantJury(), 0);
        assertEquals(tupleProces2.getPartieDefenderesse_id(), 26);
        assertEquals(tupleProces2.getPartiePoursuivant_id(), 29);
        assertEquals(tupleProces2.getDecision(), 1);
        
        assertEquals(tupleProces3.getId(), 5);
        assertEquals(tupleProces3.getJuge_id(), 28);
        assert (tupleProces3.getDate().equals(dateSQLPasse));
        assertEquals(tupleProces3.getDevantJury(), 0);
        assertEquals(tupleProces3.getPartieDefenderesse_id(), 26);
        assertEquals(tupleProces3.getPartiePoursuivant_id(), 29);
        assertEquals(tupleProces3.getDecision(), -1);
    }

    @Test
    public void creerSeance()
    {
        assertEquals(tupleSeance1.getId(), 2);
        assertEquals(tupleSeance1.getProces_id(), 2);
        assertEquals(tupleSeance1.getDate(), dateSQLFutur);

        assertEquals(tupleSeance2.getId(), 3);
        assertEquals(tupleSeance2.getProces_id(), 4);
        assertEquals(tupleSeance2.getDate(), dateSQLPasse);
    }
}